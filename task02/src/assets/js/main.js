$(document).ready(function() {
	if ($(window).width() > 767) {
		console.log($(window).width());
		$(".tab-main").each(function(){
			let tab = $(this);
			tab.find(".tab-nav-link").first().addClass("current");

			let tabID = tab.find(".tab-nav-link.current").attr("data-tab");
			tab.find(tabID).show().siblings(".tab-content").hide();
			tab.find(".tab-nav-link").click(function(e){
				let tabID = $(this).attr('data-tab');
				$(this).addClass('current').siblings().removeClass("current");
				tab.find(tabID).show().siblings(".tab-content").hide();
				e.stopPropagation();
	        	e.preventDefault();
			})
		});
	};
	if ($('.banner-slide').length){
	    $('.banner-slide').slick({
	        dots: false,
	        arrows:false,
	        infinite: true,
	        speed: 300,
	        slidesToShow: 1,
	        padding:0,
	        slidesToScroll: 1,
	        autoplay: true,
	        autoplaySpeed: 5000,
	        prevArrow: '<span class="slick-prev slick-arrow"><i class="fa fa-angle-left"><i></span>',
	        nextArrow: '<span class="slick-next slick-arrow"><i class="fa fa-angle-right"><i></span>',
	        responsive: [
	                {
	                  breakpoint: 480,
	                  settings: {
	                    arrows:false,
	                  }
	                }
	                // You can unslick at a given breakpoint now by adding:
	                // settings: "unslick"
	                // instead of a settings object
	              ]
	    });
	}
	// MENU RESPONSIVE
	$('.navbar-toggle').on('click', function (e) {
	  $(this).toggleClass('open');
	  $('body').toggleClass('menuin');
	  $('.global_nav').slideToggle();
	});

	$('.nav-ul .close > a').on('click', this, function (e) {
	  $('.navbar-toggle').trigger('click');
	});

});