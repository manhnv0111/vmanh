$(document).ready(function(e) {
		$(".accordion .accordion__content:not(:first)").hide();
		$('.accordion .accordion__subtitle').click(function(){  
			var $accordion = $(this).next();
			if ($accordion.is(':hidden') === true) {
				$(".accordion .accordion__content").slideUp();
				$('.accordion .accordion__subtitle').removeClass("active");
				$(this).addClass("active");
				$accordion.slideDown();
			} else {
				$(this).removeClass("active");
				$accordion.slideUp();
			}
		});


	//CLick tab
	  $(".p-tabs").each(function(){
	    let tab = $(this);
	    tab.find(".tab-link").first().addClass("current");

	    let tabID = tab.find(".tab-link.current").attr("data-tab");

	    tab.find(tabID).show().siblings(".tab-content").hide();

	    tab.find(".tab-link").click(function(e){
	      let tabID = $(this).attr('data-tab');
	      $(this).addClass('current').siblings().removeClass("current");

	      tab.find(tabID).show().siblings(".tab-content").hide();
	      
	      e.stopPropagation();
	          e.preventDefault();
	    })
	  })

});

