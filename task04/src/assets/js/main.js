$(document).ready(function () {
	$(window).scroll(function () {
		var scrollVal = $(window).scrollTop();
		if (scrollVal == 0) {
			$(".c-header").removeClass("hidden");
		} else if (scrollVal > 200) {
			$(".c-header").addClass("hidden");
		}
		if (scrollVal == 0) {
			$(".js-backtotop").addClass("hidden");
		} else if (scrollVal > $(window).height()) {
			$(".js-backtotop").removeClass("hidden");
		} else {
			$(".js-backtotop").addClass("hidden");
		}
	});
	$(".js-backtotop").click(function () {
		$('html,body').animate({
			scrollTop: 0
		}, 500);
	});
	if ($('.c-mainvisual__inner').length) {
		$('.c-mainvisual__inner').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 9000,
			prevArrow: '<span class="slick-prev slick-arrow"><i class="fa fa-angle-left"><i></span>',
			nextArrow: '<span class="slick-next slick-arrow"><i class="fa fa-angle-right"><i></span>',
			responsive: [{
					breakpoint: 480,
					settings: {
						arrows: false,
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			]
		});
	}
	// MENU RESPONSIVE
	$('.navbar-toggle').on('click', function (e) {
		$(this).toggleClass('open');
		$('body').toggleClass('menuin');
		$('.nav-wrap').slideToggle();
	});
	$('.js-closeMenu').on('click', function (e) {
		$('.navbar-toggle').toggleClass('open');
		$('.c-header').toggleClass('open');
		$('.nav-wrap').slideToggle();
	});
	/*=====Slick pickup=======*/
	if ($('.slick-pickup').length) {
		$('.slick-pickup').slick({
			dots: true,
			infinite: true,
			centerMode: true,
			centerPadding: '95px',
			slidesToShow: 5,
			autoplay: true,
			autoplaySpeed: 12000,
			prevArrow: '<span class="slick-prev slick-arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>',
			nextArrow: '<span class="slick-next slick-arrow"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
			responsive: [{
				breakpoint: 1920,
				settings: {
					dots: true,
					infinite: true,
					centerMode: true,
					centerPadding: '95px',
					slidesToShow: 5,
					autoplay: true,
					autoplaySpeed: 15000,
					prevArrow: '<span class="slick-prev slick-arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>',
					nextArrow: '<span class="slick-next slick-arrow"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
				}
			}, {
				breakpoint: 1024,
				settings: {
					dots: true,
					infinite: true,
					centerMode: true,
					centerPadding: '65px',
					slidesToShow: 1,
					autoplay: true,
					autoplaySpeed: 15000,
					prevArrow: '<span class="slick-prev slick-arrow"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>',
					nextArrow: '<span class="slick-next slick-arrow"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
				}
			}, {
				breakpoint: 600,
				settings: {
					dots: true,
					infinite: true,
					centerMode: true,
					centerPadding: '78px',
					slidesToShow: 1,
					autoplay: true,
					autoplaySpeed: 15000
				}
			}, {
				breakpoint: 480,
				settings: {
					dots: true,
					infinite: true,
					centerMode: true,
					centerPadding: '78px',
					slidesToShow: 1,
					autoplay: true,
					autoplaySpeed: 15000
				}
			}]
		});
	}
	$('.navbar-toggle').on('click', function (e) {
		$('.c-header').toggleClass('open');
		$('body').toggleClass('menuin');
	});
});

/*=============================CONTATC-VALIDATE==========================================*/
function checkPhone(inputtxt) {
	var numbers = /^[0-9]+$/;
	var tamp = document.getElementById(inputtxt);
	if (tamp.value.match(numbers)) {
		var numbphone = parseInt(tamp);
		if (numbphone < 10 || numbphone > 11) {
			tamp.style.border = "solid 1px #ff0000";
			tamp.value = "invalid phone number";
			return false;
		}
		return true;
	} else {
		tamp.style.border = "solid 1px #ff0000";
		tamp.value = "invalid phone number";
		return false;
	}
}

function checkEmail(idtag) {
	var mailformat = /^([\w\.])+@([a-zA-Z0-9\-])+\.([a-zA-Z]{2,4})(\.[a-zA-Z]{2,4})?$/;
	var tamp = document.getElementById(idtag);
	if (tamp.value.match(mailformat)) {
		return true;
	} else {
		tamp.style.border = "solid 1px #ff0000";
		tamp.value = "Invalid email";
		return false;
	}
}

function required(giatriName, giaTriPhone, giatriMail, gtArea) {
	if (giatriName.value == "" || giatriMail.value == "" || gtArea.value == "" || giaTriPhone.value == "") {
		giatriName.style.border = "solid 1px #ff0000";
		giaTriPhone.style.border = "solid 1px #ff0000";
		giatriMail.style.border = "solid 1px #ff0000";
		gtArea.style.border = "solid 1px #ff0000";
		return false;
	} else {
		return true;
	}
}

function checkValidate() {
	var giatriName = document.getElementById("js-input-name");
	var giaTriPhone = document.getElementById("js-input-phone");
	var giatriMail = document.getElementById("js-input-mail");
	var gtArea = document.getElementById("js-input-textarea");
	giatriName.style.border = "solid 0px #ff0000";
	giaTriPhone.style.border = "solid 0px #ff0000";
	giatriMail.style.border = "solid 0px #ff0000";
	gtArea.style.border = "solid 0px #ff0000";
	return required(giatriName, giaTriPhone, giatriMail, gtArea) && checkEmail("js-input-mail") && checkPhone("js-input-phone");
}