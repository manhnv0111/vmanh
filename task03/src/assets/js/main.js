$(document).ready(function() {
	$(".c-anchorLink__item").click(function() {
		var temp = $(this).attr('data-link');
		$('html, body').animate({
			scrollTop: $(temp).offset().top
		}, 1000);
	});

/*News*/
$(function () {
    $(".p-home1 .c-list1__card").slice(0, 4).show();
    $("#myBtnNew").on('click', function (e) {
        e.preventDefault();
        $(".p-home1 .c-list1__card:hidden").slice(0, 1).slideDown();
        if ($(".p-home1 .c-list1__card:hidden").length == 0) {
            $("#myBtnNew").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});
/*Featrue*/
$(function () {
    $(".p-home2 .c-list3").slice(0, 6).show();
    $("#myBtnFeature").on('click', function (e) {
        e.preventDefault();
        $(".p-home2 .c-list3:hidden").slice(0, 3).slideDown();
        if ($(".p-home2 .c-list3:hidden").length == 0) {
            $("#myBtnFeature").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});
/*Blog*/
$(function () {
    $(".p-home3 .c-list2").slice(0, 4).show();
    $("#myBtnBlog").on('click', function (e) {
        e.preventDefault();
        $(".p-home3 .c-list2:hidden").slice(0, 2).slideDown();
        if ($(".p-home3 .c-list2:hidden").length == 0) {
            $("#myBtnBlog").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});
});

